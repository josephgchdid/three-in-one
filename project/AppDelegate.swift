//
//  AppDelegate.swift
//  project
//
//  Created by joseph chdid on 29/12/2021.
//

import UIKit
import CallKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate , CXCallObserverDelegate{

    var callObserver =  CXCallObserver()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
    
        callObserver.setDelegate(self, queue: nil)
      
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func callObserver(_ callObserver: CXCallObserver, callChanged call: CXCall) {
    
        if call.hasEnded == true {
            print("call observer, Disconnected")
        }
        
        if call.isOutgoing == true && call.hasConnected == false {
            print("call observer, Dialing")
        }
        
        if call.isOutgoing == false && call.hasConnected == false && call.hasEnded == false {
            print("call observer, Incoming")
        }

        if call.hasConnected == true && call.hasEnded == false {
            print("call observer, Connected")
        }

    }
}


