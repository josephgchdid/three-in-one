//
//  ViewController.swift
//  project
//
//  Created by joseph chdid on 29/12/2021.
//

import UIKit

class ViewController: UIViewController {
    
    var changeLocationButton:UIBarButtonItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("view loaded")
        title = "first screen"
        
        
        self.changeLocationButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(onChangeScreen(_:)))
        
        self.navigationItem.setRightBarButton(changeLocationButton, animated: true)
        
        NotificationCenter.default.addObserver(forName: UIApplication.didEnterBackgroundNotification, object: nil, queue: nil) { (notification) in
              
            print("app did enter background")

            Background.shared.startBackgroundTasks()
           }
    }
        
    @objc func onChangeScreen(_ sender:UIBarButtonItem) {
        
        guard let vc  = storyboard?.instantiateViewController(withIdentifier: "second") as? SecondScreen else {
            fatalError()
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
       print("view will be appearing")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("view did appear on the screen!")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("view should be disappearing right about now")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("you have went to a different view!")
    }
}

