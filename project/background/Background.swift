//
//  Background.swift
//  project
//
//  Created by joseph chdid on 29/12/2021.
//

import Foundation
import UIKit


class Background {
 
    public static let shared = Background()
    
    
    private var backgroundTask: UIBackgroundTaskIdentifier = .invalid
    
    private lazy var operationQueue: OperationQueue = {
            
        let queue = OperationQueue()
        
        queue.name = "operation.queue"
        
        queue.maxConcurrentOperationCount = 1
        
        return queue
    }()
    
    private lazy var serialQueue: DispatchQueue = {
            return DispatchQueue(
                label: "operation.serialQueue"
            )
    }()
    
    private init(){
        
    }
    
    
    public func startBackgroundTasks() {
       
        serialQueue.async { [unowned self] in
                
            self.registerBackgroundTask()
                
            print("starting background tasks")
            
            let backgroundOperation = BackgroundOperation()
            
            
            let completionOperation = BlockOperation {
                
                DispatchQueue.main.async { [weak self] in
                    self?.stopBackgroundTask()
                }
            }
            
            completionOperation.addDependency(backgroundOperation)
            
            self.operationQueue.addOperations([
                backgroundOperation,completionOperation
            ], waitUntilFinished: true)
            
            self.operationQueue.waitUntilAllOperationsAreFinished()
        }
    }
    
    private func registerBackgroundTask(){
        
        stopBackgroundTask()
        
        
        print("registering background tasks")

        backgroundTask = UIApplication
        .shared.beginBackgroundTask(withName: "bgTask", expirationHandler: { [weak self] in
            
            //after system timer is done, these operations
            //are required by the OS
         
            self?.stopBackgroundTask()
        })
    }
    
    private func stopBackgroundTask(){
        
        print("stopping background tasks")
        
        UIApplication.shared.endBackgroundTask(backgroundTask)
        
        backgroundTask = .invalid
    }
    
    private func cancelAllBackgroundOperations(){
       
        print("cancelling background operations")
        
        self.operationQueue.cancelAllOperations()
    }
}
