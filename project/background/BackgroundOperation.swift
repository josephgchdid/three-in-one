//
//  BackgroundOperation.swift
//  project
//
//  Created by joseph chdid on 29/12/2021.
//

import Foundation


class BackgroundOperation : Operation {
    
    override init() {}
    
    
    override func main() {
       
        if isCancelled {
            return
        }

        print("some background task to not block main thread")
    }
}
