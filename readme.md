# Three in one project

- detecting phone calls in foreground (code can be found in app delegate)
- view lifecycle (code can be found in viewcontroller)
- detecting when app is in background and running a background task if it is (code can also be found in viewcontroller and background group)
